import React from "react";
import "./App.css";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import { PublicRoutes, PrivateRoutes } from "./routes";
import { history } from "./helpers";
import {
  LoginComponent,
  RegisterComponent,
  ForgotPasswordComponent,
} from "./auth";
import { authService } from "./services";
import landingPage from './components/landingPage/landingPage';
import { LayoutComponent } from "./components/Layouts";
import Suppliers from "./components/suppliers";
import { Typography, Row, Col } from 'antd';
import SupplierDetails from './components/supplierDetails';

const { Title } = Typography;

class App extends React.Component {
  render() {
    return (
      <Router history={history}>
        <LayoutComponent>
          <Switch>
            <Route exact path={`/login`} component={LoginComponent} />
            <Route exact path={`/register`} component={RegisterComponent} />
            <Route exact path={`/forgot-password`} exact component={ForgotPasswordComponent} />
            <Route exact path={`/services`} component={ServicesComponent} />
            <Route exact path={`/Suppliers`} component={Suppliers} />
            <Route path="/Suppliers/:id" component={SupplierDetails} />
            <Route exact path={`/industries/metal`} component={MetalIndustryComponent} />
            <Route exact path={`/industries/rubber`} component={RubberIndustryComponent} />
            <Route exact path={`/industries/plastics`} component={PlasticIndustryComponent} />
            <Route exact path={`/education/seminars`} component={SeminarsComponent} />
            <Route exact path={`/education/workshops`} component={WorkshopsComponent} />
            <Route exact path="/dashboard" component={PrivateRoutes} />
            <Route path="/" exact component={landingPage} />
          </Switch>
        </LayoutComponent>
      </Router>
    );
  }
}

export default App;

const ServicesComponent = props => {
  return (
    <React.Fragment>
      <Row align='middle' justify='center' style={{marginTop: 100}}>
        <Col>
          <Title>Services</Title>
        </Col>
      </Row>
    </React.Fragment>
  )
}

const MetalIndustryComponent = props => {
  return (
    <React.Fragment>
      <Row align='middle' justify='center' style={{marginTop: 100}}>
        <Col>
          <Title>Metal Industry</Title>
        </Col>
      </Row>
    </React.Fragment>
  )
}

const RubberIndustryComponent = props => {
  return (
    <React.Fragment>
      <Row align='middle' justify='center' style={{marginTop: 100}}>
        <Col>
          <Title>Rubber Industry</Title>
        </Col>
      </Row>
    </React.Fragment>
  )
}

const PlasticIndustryComponent = props => {
  return (
    <React.Fragment>
      <Row align='middle' justify='center' style={{marginTop: 100}}>
        <Col>
          <Title>Plastic Industry</Title>
        </Col>
      </Row>
    </React.Fragment>
  )
}

const SeminarsComponent = props => {
  return (
    <React.Fragment>
      <Row align='middle' justify='center' style={{marginTop: 100}}>
        <Col>
          <Title>Seminars</Title>
        </Col>
      </Row>
    </React.Fragment>
  )
}

const WorkshopsComponent = props => {
  return (
    <React.Fragment>
      <Row align='middle' justify='center' style={{marginTop: 100}}>
        <Col>
          <Title>Workshops</Title>
        </Col>
      </Row>
    </React.Fragment>
  )
}