import React, { Component } from "react";
import { Card, Typography } from "antd";
import { Link } from "react-router-dom";
import { PhoneOutlined } from "@ant-design/icons";

const { Text } = Typography;



class MoviesTable extends Component {
  render() {
    const { suppliers } = this.props;

    return (
      <div>
        {suppliers.map((item) => (
          <Card
            key={item._id}
            title={<Link to={`/suppliers/${item._id}`}>{item.title}{item.verified ? <Text code>√ Verified</Text> : ""}</Link>}
            extra={
              <a href="#">
                <PhoneOutlined />
              </a>
            }
            style={{ marginBottom: 15 }}
          >
            <p>Category: {item.genre.name}   |  Type: {item.CompanyType.name}   |  Year: {item.estYear}   |   Location: {item.location} </p>
            <p>{item.description}</p>
          </Card>
        ))}
      </div>
    );
  }
}

export default MoviesTable;
