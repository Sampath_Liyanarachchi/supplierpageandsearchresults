import React from "react";
import { Row, Col, Divider, Button, Progress, List, Typography } from 'antd';
import { PlusOutlined } from '@ant-design/icons';

import './dashboard.css';

const cellStyle = { padding: '8px 4px' };
const rowStyle = { minHeight: 450, justifyContent: 'center', textAlign: 'center' };
const colStyle = { justifyContent: 'center', textAlign: 'center' };

export class Dashboard extends React.Component {
  render() {
    return (
      <div style={{ minHeight: 900 }}>
       Dashboard
      </div>
    );
  }
}