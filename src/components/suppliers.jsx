import React, { Component } from "react";
import { Link } from "react-router-dom";
import SupplierList from "./supplierList";
import ListGroup from "./common/listGroup";
import Pagination from "./common/pagination";
import { getSuppliers } from "../services/fakeSupplierService";
import { getGenres } from "../services/fakeGenreService";
import { getCompanyTypes } from "../services/fakeCompanyTypeService";
import { paginate } from "../utils/paginate";
import _ from "lodash";
import SearchBox from "./searchBox";
import { Row, Col, Layout, Typography, Breadcrumb, Space, Divider } from "antd";

const { Header, Footer, Sider, Content } = Layout;
const { Title } = Typography;

class Suppliers extends Component {
  state = {
    suppliers: [],
    genres: [],
    companyType: [],
    currentPage: 1,
    pageSize: 4,
    searchQuery: "",
    selectedGenre: null,
    selectedCompanyType: null,
  };

  componentDidMount() {
    const genres = [{ _id: "", name: "All Genres" }, ...getGenres()];
    const companyType = [{ _id: "", name: "All Company Types" }, ...getCompanyTypes()];

    this.setState({ suppliers: getSuppliers(), genres, companyType });
  }

  handlePageChange = (page) => {
    this.setState({ currentPage: page });
  };

  handleGenreSelect = (genre) => {
    this.setState({ selectedGenre: genre, searchQuery: "", currentPage: 1 });
  };

  handleCompanyTypeSelect = (companyType) => {
    this.setState({ selectedCompanyType: companyType, searchQuery: "", currentPage: 1 });
  };

  handleSearch = (query) => {
    this.setState({ searchQuery: query, currentPage: 1 });
  };

  getPagedData = () => {
    const {
      pageSize,
      currentPage,
      selectedGenre,
      selectedCompanyType,
      searchQuery,
      suppliers: allSuppliers,
    } = this.state;

    let filteredBeforSearch = allSuppliers;

    if (searchQuery)
      filteredBeforSearch = allSuppliers.filter((m) =>
        m.title.toLowerCase().includes(searchQuery.toLowerCase())
      );

    let filteredBeforGenre = filteredBeforSearch;
    
    if (selectedGenre && selectedGenre._id){
      filteredBeforGenre = filteredBeforSearch.filter((m) => m.genre._id === selectedGenre._id);
    }
    
    let filtered = filteredBeforGenre;

    if (selectedCompanyType && selectedCompanyType._id){
      filtered = filteredBeforGenre.filter((m) => m.CompanyType._id === selectedCompanyType._id);
    }

    const suppliers = paginate(filtered, currentPage, pageSize);

    return { totalCount: filtered.length, data: suppliers };
  };

  render() {
    const { length: count } = this.state.suppliers;
    const { pageSize, currentPage, searchQuery } = this.state;

    if (count === 0) return <p>There are no suppliers in the database.</p>;

    const { totalCount, data: suppliers } = this.getPagedData();

    return (
      <React.Fragment>
        <Row>
          <Col span="18" offset="3">
            <Breadcrumb style={{ marginBottom: 10 }}>
              <Breadcrumb.Item>Home</Breadcrumb.Item>
              <Breadcrumb.Item>Search Field</Breadcrumb.Item>
              <Breadcrumb.Item>Search item</Breadcrumb.Item>
            </Breadcrumb>
          </Col>
        </Row>
        <Row>
          <Col span="3" offset="3">
            <Title level={3}>¤ Search within</Title>
            <SearchBox value={searchQuery} onChange={this.handleSearch} />
            <Divider style={{'backgroundColor':'#ffffff'}}/>
            <Title level={3}>¤ Filters</Title>
            <Title level={4}>Related Categories</Title>
            <ListGroup
              items={this.state.genres}
              selectedItem={this.state.selectedGenre}
              onItemSelect={this.handleGenreSelect}
            />
            <Divider />
           <Title level={4}>Company Type</Title>
            <ListGroup
              items={this.state.companyType}
              selectedItem={this.state.selectedCompanyType}
              onItemSelect={this.handleCompanyTypeSelect}
            />
          </Col>

          <Col span="12" offset="0">
            <Title>Search Page Title</Title>
            <p>Showing {totalCount} suppliers in the database.</p>
            <SupplierList suppliers={suppliers} />
            <Pagination
              itemsCount={totalCount}
              pageSize={pageSize}
              currentPage={currentPage}
              onPageChange={this.handlePageChange}
            />
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

export default Suppliers;
