import React from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import { Button } from 'antd';

const Pagination = ({ itemsCount, pageSize, currentPage, onPageChange }) => {
  const pagesCount = Math.ceil(itemsCount / pageSize);
  if (pagesCount === 1) return null;
  const pages = _.range(1, pagesCount + 1);

  return (
    <div className="pagination">
      {pages.map(page => (
        <Button type={page === currentPage ? "primary" : ""} key={page} onClick={() => onPageChange(page)}>{page}</Button>
        ))}
    </div>
  );
};

Pagination.propTypes = {
  itemsCount: PropTypes.number.isRequired,
  pageSize: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired
};

export default Pagination;

