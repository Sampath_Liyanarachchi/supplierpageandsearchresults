import React from "react";
import { Typography, Space } from 'antd';

const { Text } = Typography;

const ListGroup = ({
  items,
  textProperty,
  valueProperty,
  selectedItem,
  onItemSelect
}) => {
  return (
    <Space direction="vertical">
      {items.map(item => (
         <Text type={item === selectedItem ? "danger" : "secondary"} onClick={() => onItemSelect(item)} key={item[valueProperty]} >{item[textProperty]}</Text>
         ))}
    </Space>
 );
};

ListGroup.defaultProps = {
  textProperty: "name",
  valueProperty: "_id"
};

export default ListGroup;


/*
    <ul className="list-group">
      {items.map(item => (
        <li
          onClick={() => onItemSelect(item)}
          key={item[valueProperty]}
          className={
            item === selectedItem ? "list-group-item active" : "list-group-item"
          }
        >
          {item[textProperty]}
        </li>
      ))}
    </ul>
 */