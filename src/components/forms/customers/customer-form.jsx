import React from "react";
import { customerService } from "../../../services";
import { Form, Row, Col, Input, Button, PageHeader, message } from "antd";
import GeoSuggest from "antd-geosuggest";

export class CustomerForm extends React.Component {
  form = null;
  constructor(props) {
    super(props);
    this.state = {
      customer: null,
      addressEntered: false,
      editMode: false,
    };
  }

  async componentDidMount() {
    const editMode = !!this.props.match.params.id ? true : false;
    this.setState({ editMode });
    if (editMode) {
      let customer = await customerService.getCustomer(this.props.match.params.id);
      if (customer) {
        if (!!customer.address) {
          Object.keys(customer.address).map(key => {
            customer[key] = customer.address[key];
          });
          customer["latitude"] = customer.address.latLong.lat;
          customer["longitude"] = customer.address.latLong.lng;
        }
        this.setState({ customer });
      }
    }
  }

  getFields = () => {
    const children = [];

    for (let i = 0; i < fields.length; i++) {
      var field = fields[i];
      children.push(
        <Col span={8} key={i}>
          <Form.Item
            name={field.name}
            rules={[
              {
                required: field.required,
                message: field.label + " is Required",
              },
            ]}
          >
            <Input placeholder={field.label} />
          </Form.Item>
        </Col>
      );
    }

    return children;
  };

  getAddressFields = () => {
    const children = [];
    for (let i = 0; i < addressFields.length; i++) {
      var field = addressFields[i];

      children.push(
        <Col span={8} key={i}>
          <Form.Item
            hidden={field.hidden}
            name={field.name}
            rules={[
              {
                required: field.required,
                message: field.label + " is Required",
              },
            ]}
          >
            <Input name={field.name} placeholder={field.label} onChange={this.onAddressFieldChange} />
          </Form.Item>
        </Col>
      );
    }

    return children;
  };

  onSubmit = async () => {
    let customer = {};
    fields.forEach(field => {
      customer[field.name] = this.form.getFieldValue(field.name);
    });

    let address = {};

    addressFields.forEach(field => {
      address[field.name] = this.form.getFieldValue(field.name);
    });

    customer.address = address;
    let result = this.state.editMode
      ? await customerService.updateCustomer(this.state.customer.id, customer)
      : result = await customerService.submitCustomer(customer);

    if (!result.isSuccess) {
      message.error(result.message);
    }
    else {
      const toast = this.state.editMode ? "Customer updated" : "Customer created";
      message.success(toast);
      this.props.history.goBack();
    }
  };

  onAddressChange = (result) => {
    if (!result || result.length === 0) return;
    let newAddress = {
      streetAddress: null,
      city: null,
      state: null,
      country: null,
      postalCode: null,
      latitude: null,
      longitude: null,
      suite_Terminal_Gate: null,
    };

    addressFields.forEach((field) => (newAddress[field.name] = null));

    const address_components = result[0].gmaps.address_components;

    address_components.forEach((component) => {
      const addressType = component.types[0];
      const val = component.short_name;

      switch (addressType) {
        case "street_number":
          newAddress.streetAddress = val;
          break;
        case "route":
          newAddress.streetAddress += " " + val;
          break;
        case "locality":
          newAddress.city = val;
          break;
        case "administrative_area_level_1":
          newAddress.state = val;
          break;
        case "country":
          newAddress.country = val;
          break;
        case "postal_code":
          newAddress.postalCode = val;
          break;
        default:
          break;
      }
    });

    newAddress.latitude = result[0].lat;
    newAddress.longitude = result[0].lng;

    this.form.setFieldsValue({ "streetAddress": newAddress["streetAddress"] });
    this.form.setFieldsValue({ "city": newAddress["city"] });
    this.form.setFieldsValue({ "state": newAddress["state"] });
    this.form.setFieldsValue({ "country": newAddress["country"] });
    this.form.setFieldsValue({ "postalCode": newAddress["postalCode"] });
    this.form.setFieldsValue({ "latitude": newAddress["latitude"] });
    this.form.setFieldsValue({ "longitude": newAddress["latitude"] });

    this.setState({ addressEntered: true });
  };

  onAddressFieldChange = (e) => {
    const name = e.target.name;
    const value = e.target.value
    let pair = {};
    pair[name] = value;
    this.form.setFieldsValue(pair);
  }

  render() {
    const { editMode, customer } = this.state;
    if (editMode && !customer) return <div></div>;

    return (
      <React.Fragment>
        <PageHeader
          style={{ marginTop: "0px", paddingTop: "0px" }}
          title={(editMode ? "Edit" : "Create") + " Customer"}
        />
        <Form
          style={{ marginTop: "25px" }}
          ref={(ref) => (this.form = ref)}
          name="createCustomer"
          onFinish={this.onSubmit}
          initialValues={this.state.customer}
        >
          <Row gutter={24}>
            {this.getFields()}
            <Col span={16}>
              <Form.Item
                name={"address"}
              >
                <GeoSuggest
                  placeholder="Search Locations to automatically populate address"
                  onChange={(result) => this.onAddressChange(result)}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            {(this.state.editMode || this.state.addressEntered) && this.getAddressFields()}
          </Row>
          <Row>
            <Col
              span={24}
              style={{
                textAlign: "right",
              }}
            >
              <Button type="primary" htmlType="submit">
                Submit
                </Button>
              <Button
                style={{
                  margin: "0 8px",
                }}
                onClick={() => {
                  this.form.resetFields();
                }}
              >
                Reset
                </Button>
              <Button
                danger
                style={{ margin: "0 8px", }}
                onClick={() => this.props.history.goBack()}
              >
                Cancel
                </Button>
            </Col>
          </Row>
        </Form>
      </React.Fragment>
    );
  }
}

const fields = [
  { name: "name", label: "Customer Name", required: true },
  { name: "companyRep", label: "Customer Rep.", required: true },
  { name: "phone", label: "Phone Number", required: true },
  { name: "email", label: "Email Address", required: true },
  { name: "webAddress", label: "Website", required: false },
  { name: "fax", label: "Fax", required: false },
];

const addressFields = [
  {
    name: "streetAddress",
    label: "Street Address",
    required: true,
    hidden: false,
  },
  { name: "city", label: "City", required: true, hidden: false },
  { name: "state", label: "State", required: true, hidden: false },
  { name: "postalCode", label: "Zip Code", required: true, hidden: false },
  { name: "country", label: "Country", required: true, hidden: false },
  {
    name: "suite_Terminal_Gate",
    label: "Suite",
    required: false,
    hidden: false,
  },
  { name: "latitude", label: "Latitude", required: false, hidden: true },
  { name: "longitude", label: "Longitude", required: false, hidden: true },
];
