import React, { Component } from "react";
import { getSupplier } from "../services/fakeSupplierService";
import { getGenres } from "../services/fakeGenreService";
import {
  Row,
  Col,
  Card,
  Typography,
  Breadcrumb,
  Tooltip,
  Button,
  Divider,
} from "antd";

import { PhoneOutlined } from "@ant-design/icons";

const { Title, Text } = Typography;

 

class SupplierForm extends Component {
  state = {
    supplier: {},
    supplierGenreName: "",
    supplierCompanyTypeName: "",
    testarray:["a","b","c"]
  };

  componentDidMount() {
    const supplierId = this.props.match.params.id;
    if (supplierId === "new") return;

    const supplier = getSupplier(supplierId);
    if (!supplier) return this.props.history.replace("/not-found");

    this.setState({ supplier });
    this.setState({ supplierGenreName: supplier.genre.name });
    this.setState({ supplierCompanyTypeName: supplier.CompanyType.name });
    this.setState({ testarray: supplier.products });

    console.log(this.state.supplier.products)
  }

  render() {

    const myLists = this.state.testarray;  
    const listItems = myLists.map((myList) =>  
      <li>{myList}</li>  
    );  

    return (
      <React.Fragment>
        <Row>
          <Col span="18" offset="3">
            <Breadcrumb style={{ marginBottom: 10 }}>
              <Breadcrumb.Item>Home</Breadcrumb.Item>
              <Breadcrumb.Item>Search Field</Breadcrumb.Item>
              <Breadcrumb.Item>Company Details</Breadcrumb.Item>
              <Breadcrumb.Item>{this.state.supplier.title}</Breadcrumb.Item>
            </Breadcrumb>
          </Col>
        </Row>
        <Row>
          <Col span="18" offset="3">
            <Card style={{ marginBottom: 20 }}>
              <Row>
                <Col span="3">
                  <img
                    src={this.state.supplier.logo}
                    alt="Supplier Logo"
                    width="60%"
                  ></img>
                </Col>
                <Col span="12" offset="0">
                  <Title level={3}>{this.state.supplier.title}</Title>
                  <p>
                    {this.state.supplier.verified ? (
                      <Tooltip title="The supplier location, services and product list is verified">
                        <Text code>√ Fablanka Verified</Text>
                      </Tooltip>
                    ) : (
                      ""
                    )}{" "}
                     |  {this.state.supplierCompanyTypeName} 
                  </p>
                  <p>
                    {this.state.supplier.address}  |  <a href="#">map</a>
                  </p>
                   
                </Col>
                <Col span="9" offset="0" align="center">
                  <Button type="primary" block style={{ marginBottom: 20 }}>
                    Request a Quotation
                  </Button>
                  <p>
                    <a href="#">Call the Suppier</a>  |  
                    <a href="#">Email the Supplier</a>
                  </p>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col span="18" offset="3" style={{ marginBottom: 20 }}>
            
            <Button type="primary" danger>Profile </Button>
            <Button type="primary" danger>Certification </Button>
            <Button type="primary" danger>Additional Information </Button>
            <Button type="primary" danger>Product Information </Button>
            <Button type="primary" danger>Service Information </Button>
            <Button type="primary" danger>Request Quotation </Button>
            <Button type="primary" danger>Contact Supplier </Button>
          
            
          </Col>
        </Row>
        
        <Row>
          <Col span="18" offset="3">
            <Card>
            <Title level={3}>Business Description</Title>
              <p>{this.state.supplier.description}</p>
              <a href="#">Click here for more business description</a>
            </Card>
          </Col>
        </Row>

        <Row>
        <Col span="9" offset="3">
          <Card>
          <Title level={4}>Product & Service Offered</Title>
          <ul>{listItems}</ul>
  
            
          </Card>
        </Col>
        <Col span="9" offset="0">
        <Card>
        <Title level={4}>Business Details</Title>
        <p><b>Primary Company Type:  </b>{this.state.supplier.primaryCompanyType} </p>
        <p><b>Additional Services:  </b>{this.state.supplier.additionalActivities} </p>
        <p><b>Established Year:  </b>{this.state.supplier.estYear} </p>
        </Card>
        </Col>
        </Row>


      </React.Fragment>
    );
  }
}

export default SupplierForm;
