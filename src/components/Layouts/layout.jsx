import React, { Component } from "react";
import { withRouter } from 'react-router-dom';
import "./layout.css";

import { Layout, Menu, Row, Alert, Col, Button, Space } from "antd";
import { Navbar } from "./navbar";
import { authService } from "../../services";
import {
  UserOutlined,
  LaptopOutlined,
  NotificationOutlined,
} from "@ant-design/icons";

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

class LayoutClass extends Component {
  menuProductService = [
    <Menu.Item key='prod-service-1'>
      <a
        rel="noopener noreferrer"
        onClick={() => this.props.history.push('/services')}
      >
        Services
        </a>
    </Menu.Item>
  ];
  
  menuIndustry = [
    <Menu.Item key='industry-0'>
      <a
        rel="noopener noreferrer"
        onClick={() => this.props.history.push('/industries/metal')}
      >
        Metal Industry
        </a>
    </Menu.Item>,
    <Menu.Item key='industry-1'>
      <a
        rel="noopener noreferrer"
        onClick={() => this.props.history.push('/industries/rubber')}
      >
        Rubber Industry
        </a>
    </Menu.Item>,
    <Menu.Item key='industry-2'>
      <a
        target="_blank"
        rel="noopener noreferrer"
        onClick={() => this.props.history.push('/industries/plastics')}
      >
        Plastic Industry
        </a>
    </Menu.Item>
  ];
  
  menuMarketPlace = [
    <Menu.Item key='market-0'>
      <a
        rel="noopener noreferrer"
        href="#"
      >
        Mechanical Items
        </a>
    </Menu.Item>,
    <Menu.Item key='market-1'>
      <a
        rel="noopener noreferrer"
        href="#"
      >
        Electronic Items
        </a>
    </Menu.Item>
  ];
  
  menuEducationInnovation = [
    <Menu.Item key='ed-inno-0'>
      <a
        rel="noopener noreferrer"
        onClick={() => this.props.history.push('/education/seminars')}
      >
        Seminars
        </a>
    </Menu.Item>,
    <Menu.Item key='ed-inno-10'>
      <a
        rel="noopener noreferrer"
        onClick={() => this.props.history.push('/education/workshops')}
      >
        Workshops
        </a>
    </Menu.Item>
  ];

  constructor(props) {
    super(props);
    this.state = {
      collapsed: false,
    };
  }

  onCollapse = (collapsed) => {
    this.setState({ collapsed });
  };

  handleLogout = () => {
    authService.logout();
    this.props.history.push("/auth/login");
  };

  render() {
    return (
      <Layout>
        <Header className="header">
          <div className="logo" onClick={() => this.props.history.push('/')}>
            <img src='/images/FabLogo.png' width='200' alt="FabLanka Logo" />
          </div>
          <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["1"]}>
            <SubMenu title="Product & Service Intergration">
              {this.menuProductService}
            </SubMenu>
            <SubMenu key='for-industry' title="For Industry">{this.menuIndustry}</SubMenu>
            <SubMenu key={2} title="Market Place">{this.menuMarketPlace}</SubMenu>
            <SubMenu key={3} title="Education / Inovation">
              {this.menuEducationInnovation}
            </SubMenu>
          </Menu>
          <Space style={{ position: 'absolute', right: 0, top: 5 }}>
            <Button type='primary' onClick={() => this.props.history.push('/login')}>Log In</Button>
            <Button onClick={this.goToProfileSettings}>Register</Button>,
          </Space>
        </Header>
        {this.props.showMessage &&
          <div className="Header-Message">
            <Row>
              <Col span="24" offset="0" align="center">
                <Alert
                  message="Situational Special Messages and Links"
                  type="info"
                  closable
                />
              </Col>
            </Row>
          </div>
        }
        <Content className="site-layout-content">
          <Layout className="site-layout-background">
            {this.props.loggedIn && (
              <Sider width={200} className="site-layout-background">
                <Menu
                  mode="inline"
                  defaultSelectedKeys={["1"]}
                  defaultOpenKeys={["sub1"]}
                  style={{ height: "100%", borderRight: 0 }}
                >
                  <SubMenu key="sub1" icon={<UserOutlined />} title="subnav 1">
                    <Menu.Item key="1">option1</Menu.Item>
                    <Menu.Item key="2">option2</Menu.Item>
                    <Menu.Item key="3">option3</Menu.Item>
                    <Menu.Item key="4">option4</Menu.Item>
                  </SubMenu>
                  <SubMenu
                    key="sub2"
                    icon={<LaptopOutlined />}
                    title="subnav 2"
                  >
                    <Menu.Item key="5">option5</Menu.Item>
                    <Menu.Item key="6">option6</Menu.Item>
                    <Menu.Item key="7">option7</Menu.Item>
                    <Menu.Item key="8">option8</Menu.Item>
                  </SubMenu>
                  <SubMenu
                    key="sub3"
                    icon={<NotificationOutlined />}
                    title="subnav 3"
                  >
                    <Menu.Item key="9">option9</Menu.Item>
                    <Menu.Item key="10">option10</Menu.Item>
                    <Menu.Item key="11">option11</Menu.Item>
                    <Menu.Item key="12">option12</Menu.Item>
                  </SubMenu>
                </Menu>
              </Sider>
            )}
            <Content className='main-content-area'>
              {this.props.children}
            </Content>
            <Footer
              style={{ textAlign: "center", backgroundColor: 'black', color: 'white', marginTop: 50 }}
            >
              FabLanka ©2020
        </Footer>
          </Layout>
        </Content>
      </Layout>
    );
  }
}

export const LayoutComponent = withRouter(LayoutClass);