import React, { Component } from "react";
import { history } from "../../helpers";
import "./layout";

import {
  message,
  Menu,
  PageHeader,
  Space,
  Button,
  Popconfirm,
  Upload,
  Dropdown,
  Avatar,
} from "antd";
import { UserOutlined } from "@ant-design/icons";

export class Navbar extends Component {
  constructor(props) {
    super(props);
  }

  goToProfileSettings = () => this.props.history.push('/profile')

  render() {
    return (
      <PageHeader
        ghost={false}
        title="FabLanka Portal"
        extra={[
          <Button onClick={this.goToProfileSettings} key={1} icon={<Avatar style={{ backgroundColor: '#87d068' }} src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />} />,
          <Button key={2} danger onClick={() => this.props.handleLogout()}>Log Out</Button>
        ]}
      />
    );
  }
}
