import React, { useState } from "react";
import {
  Row,
  Col,
  Menu,
  Dropdown,
  Alert,
  Typography,
  Input,
  Select,
  Card,
} from "antd";
import { DownOutlined } from "@ant-design/icons";
import { useHistory } from "react-router-dom";

import "./landingPage.css";

const { Search } = Input;
const { Option } = Select;
const { Title, Text } = Typography


function LandingPage() {
  const [searchType, setSearchType] = useState("Professional Services");
  const history = useHistory();

  function onSelectSearchType(value) {
    setSearchType(value);
  }

  function handleSearchClick(value) {
    console.log(value);
    history.push("/Suppliers");
  }

  const selectBefore = (
    <Select onChange={onSelectSearchType} defaultValue={searchType} className="select-before" size='large'>
      <Option value="Professional Services">Professional Services</Option>
      <Option value="Engineering Products">Engineering Products</Option>
      <Option value="CAD Models">CAD Models</Option>
    </Select>
  );

  return (
    <React.Fragment>
      <div className="Content-logo">
        <Row>
          <Col span="24" offset="0" align="center">
            <Title style={{ fontSize: '4em', marginBottom: 5 }}>FABNET</Title>
            <Text level={4} style={{ fontSize: '1.5em' }}>
              For Suppliers <span className='subtitleSeparator'>|</span> KnowledgeBase <span className='subtitleSeparator'>|</span> Insights, Tools, and More...
            </Text>
          </Col>
        </Row>
      </div>
      <div className="Content-InfoText">
        <Row>
          <Col span="24" offset="0" align="center">
            <h1>Bring Your Ideas to life</h1>
          </Col>
        </Row>
      </div>
      <div className="Content-SearchBar">
        <Input.Group size='large'>
          <Row justify='center'>
            <Col span={16}>
              {selectBefore}
              <Search
                placeholder={"Search for " + searchType}
                style={{ width: '70%' }}
                enterButton='Search'
                size='large'
                //onSearch={handleSearchClick(value) => console.log(value)}
                //onSearch={(value) => handleSearchClick(value)}
                onSearch={handleSearchClick}
              />
            </Col>
          </Row>
        </Input.Group>
      </div>
      <div className="Content-KnowledgeBase">
        <Row justify='center'>
          <Col span="12" align="center">
            <Dropdown overlay={<Menu>
              <Menu.Item key="1">3D Printing</Menu.Item>
              <Menu.Item key="2">CNC Manufacturing</Menu.Item>
            </Menu>}>
              <a
                className="ant-dropdown-link"
                onClick={(e) => e.preventDefault()}
              >
                Engineering Knowledge Base <DownOutlined />
              </a>
            </Dropdown>
          </Col>
        </Row>
      </div>
      <div className="Content-Highlights">
        <Row justify='space-around'>
          <Col align="center">
            <Card style={{ width: 400 }}>
              <img src='/images/Highlight.png' alt="FabLanka Logo" width="60%"></img>;
              <h2>Highlight 01</h2>
              <p>Card content</p>
            </Card>
          </Col>
          <Col span="6" offset="0" align="center">
            <Card style={{ width: 400 }}>
              <img src='/images/Highlight.png' alt="FabLanka Logo" width="60%"></img>;
              <h2>Highlight 02</h2>
              <p>Card content</p>
            </Card>
          </Col>
          <Col span="6" offset="0" align="center">
            <Card style={{ width: 400 }}>
              <img src='/images/Highlight.png' alt="FabLanka Logo" width="60%"></img>;
              <h2>Highlight 03</h2>
              <p>Card content</p>
            </Card>
          </Col>
        </Row>
      </div>
    </React.Fragment>
  );
}

export default LandingPage;