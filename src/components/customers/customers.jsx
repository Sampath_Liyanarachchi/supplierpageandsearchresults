import React from "react";
import { customerService } from "../../services";
import { Table, Tooltip, Progress, Space, PageHeader, Button } from "antd";
import { withToastManager } from 'react-toast-notifications';

class CustomersComponent extends React.Component {
  columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      render: (name, customer) => <a onClick={() => this.viewCustomer(customer.id)}>{name}</a>,
    },
    {
      title: "Address",
      dataIndex: "address",
      key: "address",
      render: (address) => <p>{`${address.city}, ${address.state_District}`}</p>,
    },
    {
      title: "Phone",
      dataIndex: "phone",
      key: "phone",
      render: (phone) => <p>{phone}</p>,
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
      render: (email) => <p>{email}</p>,
    },
    {
      title: "Website",
      dataIndex: "website",
      key: "website",
      render: (website) => <p>{website}</p>,
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <Space size="middle">
          <Button
            onClick={() => this.editCustomer(record.id)}
            type="primary"
            style={{width: 50}}
            size='small'
          >
            Edit
          </Button>
          <Button
            onClick={() => this.deleteCustomer(record.id)}
            danger
            type="primary"
            size='small'
          >
            Delete
          </Button>
        </Space>
      ),
    },
  ];

  constructor(props) {
    super(props);
    this.state = {
      customers: []
    }
  }

  async componentDidMount() {
    // let customers = await customerService.fetchCustomers();
    // customers.map((customer, idx) => customer.key = idx);
    // this.setState({ customers });
  }

  viewCustomer = (id) => this.props.history.push(`/customers/${id}`);

  deleteCustomer = async (id) => {
    const result = await customerService.deleteCustomer(id);
    if (!result.isSuccess) {
      this.props.toastManager.add(result.message, { appearance: 'error', autoDismiss: true });
    }
    else {
      this.props.toastManager.add("Customer deleted", { appearance: 'success', autoDismiss: true });
      let customers = await customerService.getCustomers();
      customers.map((customer, idx) => customer.key = idx);
      this.setState({ customers });
    }
  }

  editCustomer = (id) => this.props.history.push(`/customers/edit/${id}`);
  createCustomer = () => this.props.history.push('/customers/create');

  render() {
    const { customers } = this.state;

    return (
      <React.Fragment>
        <PageHeader
          style={{ marginTop: "0px", paddingTop: "0px" }}
          title="Customers"
          extra={[
            <Button key="1" type="primary" onClick={this.createCustomer}>Create</Button>
          ]}
        />
        <Table
          columns={this.columns}
          dataSource={customers}
          scroll={{ x: 1000, y: 800 }}
        />
      </React.Fragment>
    );
  }
}

const Customers = withToastManager(CustomersComponent);
export { Customers };