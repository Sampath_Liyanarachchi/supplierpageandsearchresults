import React from "react";
import { customerService } from "../../services";
import {
  PageHeader,
  Button,
  Card,
  Modal,
  Input,
  Form,
  Table,
  Space,
  message,
  Dropdown,
  Menu,
  Divider,
} from "antd";
import { DownOutlined } from '@ant-design/icons';
import { withToastManager } from "react-toast-notifications";

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};

class CustomerComponent extends React.Component {
  inviteForm = null;
  form = null;
  teacherColumns = [
    {
      title: "First Name",
      dataIndex: "firstName",
      key: "firstName",
      render: (firstName) => <p>{firstName}</p>,
    },
    {
      title: "Last Name",
      dataIndex: "lastName",
      key: "lastName",
      render: (lastName) => <p>{lastName}</p>,
    },
    {
      title: "Phone",
      dataIndex: "phone",
      key: "phone",
      render: (phone) => <p>{phone}</p>,
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
      render: (email) => <p>{email}</p>,
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <Space size="middle">
          <a onClick={() => this.editUser(record.id)}>Edit</a>
          <a onClick={() => this.deleteUser(record.id)}>Delete</a>
        </Space>
      ),
    },
  ];
  studentColumns = [
    {
      title: "First Name",
      dataIndex: "firstName",
      key: "firstName",
      render: (firstName) => <p>{firstName}</p>,
    },
    {
      title: "Last Name",
      dataIndex: "lastName",
      key: "lastName",
      render: (lastName) => <p>{lastName}</p>,
    },
    {
      title: "Phone",
      dataIndex: "phone",
      key: "phone",
      render: (phone) => <p>{phone}</p>,
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
      render: (email) => <p>{email}</p>,
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <Space size="middle">
          <a onClick={() => this.editUser(record.id)}>Edit</a>
          <a onClick={() => this.deleteUser(record.id)}>Delete</a>
        </Space>
      ),
    },
  ];
  constructor(props) {
    super(props);
    this.state = {
      customer: null,
      loading: true,
      inviteModalVisible: false,
      fieldsEditable: false,
    };
  }

  async componentDidMount() {
    if (!this.props.match.params.id) this.props.history.goBack();
    this.fetchCustomer(this.props.match.params.id);
  }

  fetchCustomer = async (id) => {
    const customer = await customerService.fetchCustomer(id);
    if (!!customer) {
      this.setState({ customer, loading: false });

      let pair = {};
      pair['name'] = customer.name;
      pair['representative'] = customer.rep;

      this.form.resetFields();
      this.form.setFieldsValue(pair);
    }
  }

  deleteCustomer = async () => {
    const result = await customerService.deleteCustomer(this.state.customer.id);
    if (!result.isSuccess) {
      this.props.toastManager.add(result.message, {
        appearance: "error",
        autoDismiss: true,
      });
    } else {
      this.props.toastManager.add("Customer deleted", {
        appearance: "success",
        autoDismiss: true,
      });
      this.props.goBack();
    }
  };

  editUser = (id) => {};
  deleteUsers = (id) => {};

  inviteUser = async() => {
    const email = this.inviteForm.getFieldValue("email");
    if (!email) {
        message.error("Please enter an Email Address", 1);
        return;
    }

    const result = await customerService.inviteUser(this.state.customer.id, email);
    if (result.isSuccess) {
        message.success("Invited User");
        this.setState({inviteModalVisible: false});
        this.inviteForm.resetFields();
        this.inviteForm = null;
    }
    else {
        message.error(result.message);
    }
  };

  handleMenuClick = (value) => {
    const key = value.key;
    switch (key) {
      case 'edit':
        this.setState({fieldsEditable: true});
        break;
      case 'addTeacher':
        console.log('add Teacher')
        break;
      case 'addStudent':
        console.log('add Student')
        break;
      case 'createCurriculum':
        console.log('add Curriculum')
        break;
      default:
        break;
    }
  }

  createCurriculum = () => {

  }

  addTeacher = () => {

  }

  addStudent = () => {

  }

  cancelEdit = () => {
    this.setState({fieldsEditable: false});
    this.fetchCustomer(this.state.customer.id);
  }

  updateCustomer = () => {

  }

  getAddressFields = () => {
    const children = [];
    for (let i = 0; i < addressFields.length; i++) {
      var field = addressFields[i];

      children.push(
        <Form.Item
            name={field.key}
            label={field.value}
            hasFeedback
            key={i}
            rules={[
              {
                required: field.required,
                message: field.value + " is Required",
              },
            ]}
          >
            <Input disabled={!this.state.fieldsEditable} name={field.key} placeholder={field.value} />
          </Form.Item>
      );
    }

    return children;
  };

  render() {
    const { customer, loading, fieldsEditable } = this.state;

    if (!customer) return <div></div>;

    return (
      <React.Fragment>
        <PageHeader
          style={{ marginTop: "0px", paddingTop: "0px" }}
          title={customer.name} 
          extra={[
            !fieldsEditable && (
            <Dropdown overlay={
              <Menu onClick={this.handleMenuClick}>
                {actionsMenu.map(action => <Menu.Item key={action.key}>{action.title}</Menu.Item>)}
              </Menu>
            }>
              <Button type="primary">
                Actions <DownOutlined />
              </Button>
            </Dropdown>),
            fieldsEditable && <Button type="primary">Save</Button>,
            fieldsEditable && <Button type="primary" danger onClick={this.cancelEdit}>Cancel</Button>
          ]}
        />
        <Card style={{ width: "75%", marginTop: 16 }} loading={loading}>
          <Form {...formItemLayout} ref={_form => this.form = _form}>
            <Form.Item 
               name="name"
               label="Name"
               rules={[
                 {
                   required: fieldsEditable ? true : false,
                   message: 'Name is Required',
                 },
               ]}
            >
              <Input placeholder="Name" disabled={!fieldsEditable} />
            </Form.Item>
            <Form.Item label="Email" name="email">
              <Input placeholder={customer.email} disabled={!fieldsEditable} />
            </Form.Item>
            <Form.Item label="Phone" name="phone">
              <Input placeholder={customer.phone} disabled={!fieldsEditable} />
            </Form.Item>
            <Form.Item label="Fax" name="fax">
              <Input placeholder={customer.fax} disabled={!fieldsEditable} />
            </Form.Item>
            <Form.Item label="Website" name="website">
              <Input placeholder={customer.website} disabled={!fieldsEditable} />
            </Form.Item>
            <Divider />
            {this.getAddressFields()}
          </Form>
        </Card>
      </React.Fragment>
    );
  }
}

const Customer = withToastManager(CustomerComponent);
export { Customer };

const actionsMenu = [
  {title: "Edit", key: 'edit'},
  {title: "Create Curriculum", key: 'createCurriculum'},
  {title: "Add Teacher", key: 'addTeacher'},
  {title: "Add Student", key: 'addStudent'}
]

const addressFields = [
  { key: 'house', value: "House Number", required: true },
  { key: 'street', value: "Street Address", required: true },
  { key: 'city', value: "City", required: true },
  { key: 'state_Province', value: "State/Province", required: true },
  { key: 'country', value: "Country", required: true },
]