import * as genresAPI from "./fakeGenreService";

const Supplier = [
  {
    _id: "5b21ca3eeb7f6fbccd471815",
    title: "Terminator Metal Supplier",
    verified: true,
    description: "Custom manufacturer of hydraulic, electrical and mechanical actuators for the military and commercial aircraft, aerospace and defense industries. Various machining capabilities include 3, 4 and 5-axis horizontal vertical machining, 3 and 5-axis gantry milling, gun drilling, CNC lathe work, vertical horizontal honing, OD and ID grinding and turning. Meets AS9100D standards.",
    logo: "/images/ImageNotFound.jpg",
    genre: { _id: "5b21ca3eeb7f6fbccd471818", name: "Mechanical" },
    location: "USA",
    address: "No15, Duke Road, New York, USA",
    products:["Box Bars", "Stainless Steel Rods", "Metal Sheets", "Metal Casting"],
    estYear: "2018",
    primaryCompanyType: "Custom Manufacture",
    additionalActivities: "Manufacturer, Service Company",
    CompanyType: { _id: "6f89ca3eeb7f6fbccd471818", name: "Manufacturer" }
  },
  {
    _id: "5b21ca3eeb7f6fbccd471816",
    title: "ABC Pump Suppler",
    verified: false,
    description: "Custom manufacturer of hydraulic, electrical and mechanical actuators for the military and commercial aircraft, aerospace and defense industries. Various machining capabilities include 3, 4 and 5-axis horizontal vertical machining, 3 and 5-axis gantry milling, gun drilling, CNC lathe work, vertical horizontal honing, OD and ID grinding and turning. Meets AS9100D standards.",
    logo: "/images/ImageNotFound.jpg",
    genre: { _id: "5b21ca3eeb7f6fbccd471818", name: "Mechanical" },
    location: "USA",
    address: "No15, Lake Road, Texas, USA",
    products:["Nuts", "Motors", "Taps"],
    estYear: "2008",
    primaryCompanyType: "Custom Distributor",
    additionalActivities: "logistic and organization support",
    CompanyType: { _id: "6f89ca3eeb7f6fbccd471814", name: "Distributor" }
  },
  {
    _id: "5b21ca3eeb7f6fbccd471817",
    title: "Arc Building Equipments",
    verified: true,
    description: "Custom manufacturer of hydraulic, electrical and mechanical actuators for the military and commercial aircraft, aerospace and defense industries. Various machining capabilities include 3, 4 and 5-axis horizontal vertical machining, 3 and 5-axis gantry milling, gun drilling, CNC lathe work, vertical horizontal honing, OD and ID grinding and turning. Meets AS9100D standards.",
    logo: "/images/ImageNotFound.jpg",
    genre: { _id: "5b21ca3eeb7f6fbccd471820", name: "Civil" },
    location: "USA",
    address: "58/120, Duke Place, Miami, USA",
    products:["Nuts", "Motors", "Taps"],
    estYear: "2005",
    primaryCompanyType: "State Manufacturer",
    additionalActivities: "Tenders and Report Development",
    CompanyType: { _id: "6f89ca3eeb7f6fbccd471818", name: "Manufacturer" }
  },
  {
    _id: "5b21ca3eeb7f6fbccd471819",
    title: "HK Motors",
    verified: false,
    description: "Custom manufacturer of hydraulic, electrical and mechanical actuators for the military and commercial aircraft, aerospace and defense industries. Various machining capabilities include 3, 4 and 5-axis horizontal vertical machining, 3 and 5-axis gantry milling, gun drilling, CNC lathe work, vertical horizontal honing, OD and ID grinding and turning. Meets AS9100D standards.",
    logo: "/images/ImageNotFound.jpg",
    genre: { _id: "5b21ca3eeb7f6fbccd471814", name: "Electrical" },
    location: "India",
    address: "No15, Duke Road, New delhi, India",
    products:["Nuts", "Motors", "Taps"],
    estYear: "2018",
    primaryCompanyType: "Hydraulic Manufacturer",
    additionalActivities: "Seller of Hydraulic equipment",
CompanyType: { _id: "6f89ca3eeb7f6fbccd471818", name: "Manufacturer" }
  },
  {
    _id: "5b21ca3eeb7f6fbccd47181a",
    title: "Airplane Electronics",
    verified: false,
    description: "Custom manufacturer of hydraulic, electrical and mechanical actuators for the military and commercial aircraft, aerospace and defense industries. Various machining capabilities include 3, 4 and 5-axis horizontal vertical machining, 3 and 5-axis gantry milling, gun drilling, CNC lathe work, vertical horizontal honing, OD and ID grinding and turning. Meets AS9100D standards.",
    logo: "/images/ImageNotFound.jpg",
    genre: { _id: "5b21ca3eeb7f6fbccd471814", name: "Electrical" },
    location: "Sri_Lanka",
    address: "N57/44, Thimbirigasyaya Road, Hendala, Sri Lanka",
    products:["Nuts", "Motors", "Taps"],
    estYear: "1992",
    primaryCompanyType: "International Distributor",
    additionalActivities: "Tenders and Report Development",
    CompanyType: { _id: "6f89ca3eeb7f6fbccd471814", name: "Distributor" }
  },
  {
    _id: "5b21ca3eeb7f6fbccd47181b",
    title: "Tronic Center",
    verified: false,
    description: "Custom manufacturer of hydraulic, electrical and mechanical actuators for the military and commercial aircraft, aerospace and defense industries. Various machining capabilities include 3, 4 and 5-axis horizontal vertical machining, 3 and 5-axis gantry milling, gun drilling, CNC lathe work, vertical horizontal honing, OD and ID grinding and turning. Meets AS9100D standards.",
    logo: "/images/ImageNotFound.jpg",
    genre: { _id: "5b21ca3eeb7f6fbccd471814", name: "Electrical" },
    location: "India",
    address: "No15, Duke Road, New York, USA",
    products:["Nuts", "Motors", "Taps"],
    estYear: "1998",
    primaryCompanyType: "State Manufacturer",
    additionalActivities: "Tenders and Report Development",
    CompanyType: { _id: "6f89ca3eeb7f6fbccd471818", name: "Manufacturer" }
  },
  {
    _id: "5b21ca3eeb7f6fbccd47181e",
    title: "Gone Building Equipments",
    verified: false,
    description: "Custom manufacturer of hydraulic, electrical and mechanical actuators for the military and commercial aircraft, aerospace and defense industries. Various machining capabilities include 3, 4 and 5-axis horizontal vertical machining, 3 and 5-axis gantry milling, gun drilling, CNC lathe work, vertical horizontal honing, OD and ID grinding and turning. Meets AS9100D standards.",
    logo: "/images/ImageNotFound.jpg",
    genre: { _id: "5b21ca3eeb7f6fbccd471820", name: "Civil" },
    location: "Sri_Lanka",
    address: "No15, Duke Road, Galle, Sri Lanka",
    products:["Nuts", "Motors", "Taps"],
    estYear: "2018",
    primaryCompanyType: "International Distributor",
    additionalActivities: "Tenders and Report Development",
    CompanyType: { _id: "6f89ca3eeb7f6fbccd471814", name: "Distributor" }
  },
  {
    _id: "5b21ca3eeb7f6fbccd47181f",
    title: "The Sixth Sense Constructions",
    verified: false,
    description: "Custom manufacturer of hydraulic, electrical and mechanical actuators for the military and commercial aircraft, aerospace and defense industries. Various machining capabilities include 3, 4 and 5-axis horizontal vertical machining, 3 and 5-axis gantry milling, gun drilling, CNC lathe work, vertical horizontal honing, OD and ID grinding and turning. Meets AS9100D standards.",
    logo: "/images/ImageNotFound.jpg",
    genre: { _id: "5b21ca3eeb7f6fbccd471820", name: "Civil" },
    location: "China",
    address: "85 main street china",
    products:["Nuts", "Motors", "Taps"],
    estYear: "2018",
    primaryCompanyType: "Product Designer",
    additionalActivities: "Topology Optimization and FEA",
    CompanyType: { _id: "6f89ca3eeb7f6fbccd471820", name: "Designer" }
  },
  {
    _id: "5b21ca3eeb7f6fbccd471821",
    title: "Avengers Industrial Services",
    verified: false,
    description: "Custom manufacturer of hydraulic, electrical and mechanical actuators for the military and commercial aircraft, aerospace and defense industries. Various machining capabilities include 3, 4 and 5-axis horizontal vertical machining, 3 and 5-axis gantry milling, gun drilling, CNC lathe work, vertical horizontal honing, OD and ID grinding and turning. Meets AS9100D standards.",
    logo: "/images/ImageNotFound.jpg",
    genre: { _id: "5b21ca3eeb7f6fbccd471818", name: "Mechanical" },
    location: "China",
    address: "Room 85, Avengers Tower, China",
    products:["Nuts", "Motors", "Taps"],
    estYear: "2018",
    primaryCompanyType: "International Manufacturer",
    additionalActivities: "Tenders and Report Development",
    CompanyType: { _id: "6f89ca3eeb7f6fbccd471818", name: "Manufacturer" }
  }
];

export function getSuppliers() {
  return Supplier;
}

export function getSupplier(id) {
  return Supplier.find(m => m._id === id);
}


