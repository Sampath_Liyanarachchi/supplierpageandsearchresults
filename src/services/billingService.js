import axios from '../config/API';
import { trackPromise } from 'react-promise-tracker';

export const billingService = {
    getBilling,
};

async function getBilling() {
    try {
        const result = await trackPromise(axios.get('/billing'));
        if (!result.data.isSuccess) return [];

        return result.data.dto;
    }
    catch(err) {
        console.log(err);
        return [];
    }
}