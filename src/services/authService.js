import { BehaviorSubject } from 'rxjs';
import axios from '../config/API';
import { trackPromise } from 'react-promise-tracker';

const currentUserSubject = new BehaviorSubject(JSON.parse(localStorage.getItem('currentUser')));

export const authService = {
    login,
    logout,
    currentUser: currentUserSubject.asObservable(),
    get currentUserValue () { return currentUserSubject.value }
};

async function login(email, password) {
    try {
        const result = await trackPromise(axios.post('/auth/login', { email, password }));
        const data = result.data;
        if (data.isSuccess) {
            localStorage.setItem('currentUser', JSON.stringify(data.dto));
            currentUserSubject.next(data.dto);
            return data.dto;
        }
    }
    catch(err) {
        return currentUserSubject.next(null);
    }
}

function logout() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem("roles");
    currentUserSubject.next(null);
}