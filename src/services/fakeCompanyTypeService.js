export const companyTypes = [
  { _id: "6f89ca3eeb7f6fbccd471818", name: "Manufacturer" },
  { _id: "6f89ca3eeb7f6fbccd471814", name: "Distributor" },
  { _id: "6f89ca3eeb7f6fbccd471820", name: "Designer" }
];

export function getCompanyTypes() {
  return companyTypes.filter(g => g);
}
