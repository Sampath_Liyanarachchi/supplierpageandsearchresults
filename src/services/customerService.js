import axios from '../config/API';
import { trackPromise } from 'react-promise-tracker';

export const customerService = {
    fetchCustomers,
    submitCustomer,
    deleteCustomer,
    fetchCustomer,
    updateCustomer,
    inviteUser,
    getCustomerAddresses,
    submitNewCustomerAddress,
};

async function fetchCustomers() {
    try {
        const result = await trackPromise(axios.get('/montessories'));
        if (!result.data.isSuccess) return [];

        return result.data.dto;
    }
    catch(err) {
        console.log(err);
        return [];
    }
}

async function fetchCustomer(id) {
    try {
        const result = await trackPromise(axios.get(`/montessories/${id}`));
        if (!result.data.isSuccess) return null;

        return result.data.dto;
    }
    catch(err) {
        console.log(err);
        return { isSuccess: false, message: "Unable to get customer"};
    }
}

async function submitCustomer(customer) {
    try {
        return (await trackPromise(axios.post('/customers', customer))).data;
    }
    catch(err) {
        console.log(err);
        return { isSuccess: false, message: "Unable to create customer"};
    }
}

async function updateCustomer(id, customer) {
    try {
        return (await trackPromise(axios.put(`/customers/${id}`, customer))).data;
    }
    catch(err) {
        console.log(err);
        return { isSuccess: false, message: "Unable to create customer"};
    }
}

async function deleteCustomer(customerId) {
    try {
        return (await trackPromise(axios.delete(`/customers/${customerId}`))).data;
    }
    catch(err) {
        console.log(err);
        return { isSuccess: false, message: "Unable to delete customer"};
    }
}

async function inviteUser(customerId, email) {
    try {
        return (await trackPromise(axios.post(`/customers/${customerId}/users/invite/${email}`))).data;
    }
    catch(err) {
        console.log(err);
        return { isSuccess: false, message: "Unable to send invite"};
    }
}

async function getCustomerAddresses(id) {
    try {
        const result = await trackPromise(axios.get(`/customers/${id}/locations`));
        return result.data;
    }
    catch(err) {
        console.log(err);
        return { isSuccess: false, message: "Unable to get customer"};
    }
}

async function submitNewCustomerAddress(id, address) {
    try {
        const result = await trackPromise(axios.post(`/customers/${id}/locations`, address));
        return result.data;
    }
    catch(err) {
        console.log(err);
        return { isSuccess: false, message: "Unable to create new customer address"};
    }
}