export const genres = [
  { _id: "5b21ca3eeb7f6fbccd471818", name: "Mechanical" },
  { _id: "5b21ca3eeb7f6fbccd471814", name: "Electrical" },
  { _id: "5b21ca3eeb7f6fbccd471820", name: "Civil" }
];

export function getGenres() {
  return genres.filter(g => g);
}
