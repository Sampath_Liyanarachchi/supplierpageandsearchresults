import axios from '../config/API';
import { trackPromise } from 'react-promise-tracker';

export const commonService = {
    getCreateOptions,
    getDispatchCreateOptions,
};

async function getCreateOptions() {
    try {
        const result = await trackPromise(axios.get('/loads/create/options'));
        if (!result.data.isSuccess) return [];

        return result.data.dto;
    }
    catch(err) {
        console.log(err);
        return [];
    }
}

async function getDispatchCreateOptions() {
    try {
        const result = await trackPromise(axios.get('/dispatches/create/options'));
        return result.data;
    }
    catch(err) {
        return { isSuccess: false, message: "Unable to fetch options" };
    }
}