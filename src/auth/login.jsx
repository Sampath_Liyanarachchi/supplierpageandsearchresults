import React from "react";
import "./auth.css";
import {
  Form,
  Input,
  Button,
  Checkbox,
  Row,
  Col,
  Card,
  PageHeader,
} from "antd";
import { authService } from "../services";

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

export class LoginComponent extends React.Component {

  componentDidMount() {
    if (!!authService.currentUserValue) {
      this.props.history.push('/dashboard');
    }
  }

  onSubmit = (values) => {
    if (!values.username || !values.password) return;
    authService.login(values.username, values.password)
    .then(result => {
        if (!!result) {
            this.props.history.push('/');
        }
        else {
          // window.location.reload();
        }
    });
  };

  onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  render() {
    return (
      <React.Fragment>
        <Row
          type="flex"
          justify="center"
          align="middle"
          style={{ height: "100vh" }}
        >
          <Col span={18}>
            <img src="/images/login-page.jpg" width='100%' />
          </Col>
          <Col span={6}>
            <React.Fragment>
              <PageHeader
                className="site-page-header-responsive"
                title="Sign In"
              />
              <Form
                {...layout}
                style={{ marginRight: "10px" }}
                name="basic"
                initialValues={{
                  remember: true,
                }}
                onFinish={this.onSubmit}
                onFinishFailed={this.onFinishFailed}
              >
                <Form.Item
                  label="Username"
                  name="username"
                  rules={[
                    {
                      required: true,
                      message: "Please input your username!",
                    },
                  ]}
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  label="Password"
                  name="password"
                  rules={[
                    {
                      required: true,
                      message: "Please input your password!",
                    },
                  ]}
                >
                  <Input.Password />
                </Form.Item>

                <Form.Item
                  {...tailLayout}
                  name="remember"
                  valuePropName="checked"
                >
                  <Checkbox>Remember me</Checkbox>
                </Form.Item>

                <Form.Item {...tailLayout}>
                  <Button type="primary" htmlType="submit">
                    Submit
                  </Button>
                </Form.Item>
              </Form>
            </React.Fragment>
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}
