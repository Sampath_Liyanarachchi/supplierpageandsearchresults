import React, { Component, Fragment } from "react";
import { Route, Switch, withRouter, Redirect } from "react-router-dom";
import { uniqBy } from "lodash";
import { rolesConfig } from "../config";
import * as Routes from "./index";
import { NotFound } from "../components";
import { authService } from "../services";
import { LayoutComponent } from "../components/Layouts";

export class PrivateRoutes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allowedRoutes: [],
    };
  }

  componentDidMount() {
    if (!authService.currentUserValue) {
      authService.logout();
      this.props.history.push("/login");
      return;
    }
    let roles = authService.currentUserValue.roles;
    if (roles) {
      roles = ["common", ...roles];

      let allowedRoutes = roles.reduce((acc, role) => {
        return [...acc, ...rolesConfig[role].routes];
      }, []);

      allowedRoutes = uniqBy(allowedRoutes, "url");
      this.setState({ allowedRoutes });
    } else {
      this.props.history.push("/dashboard");
    }
  }

  render() {
    if (this.props.location.pathname === '/') {
      return <Redirect to="/dashboard" />;
    }
    
    return (
      <Fragment>
        <LayoutComponent
          routes={this.state.allowedRoutes}
          path={this.props.match.path}
          history={this.props.history}
          {...this.props}
        >
          <Switch>
            {this.state.allowedRoutes.map((route) => {
              if (!route.isGroup) {
                return (
                  <Route
                    exact
                    key={route.url}
                    component={Routes[route.component]}
                    path={`${route.url}`}
                  />
                );
              } else {
                return route.children.map((subRoute) => (
                  <Route
                    exact
                    key={subRoute.url}
                    component={Routes[subRoute.component]}
                    path={`${route.url}/${subRoute.url}`}
                  />
                ));
              }
            })}
            <Route component={NotFound} />
          </Switch>
        </LayoutComponent>
      </Fragment>
    );
  }
}

export default withRouter(PrivateRoutes);
