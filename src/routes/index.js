import React from "react";
import { Customers, Customer } from '../components/customers';
import { ProfileSettings } from '../components/profile';
import { Billing } from '../components/billing';
import { Dashboard } from '../components/dashboard';
import { CustomerForm } from '../components/forms';

export * from "./PublicRoutes";
export * from "./PrivateRoutes";

export {
  Dashboard,
  Customers,
  Billing,
  CustomerForm,
  Customer,
  ProfileSettings,
};
