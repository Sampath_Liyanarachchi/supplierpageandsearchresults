import React from "react";
import { Route, Switch } from "react-router-dom";
import {
  LoginComponent,
  RegisterComponent,
  ForgotPasswordComponent,
} from "../auth";

export class PublicRoutes extends React.Component {
  render() {
    return (
      <Switch>
        <Route path={`${this.props.match.path}/login`} component={LoginComponent} />
        <Route path={`${this.props.match.path}/register`} component={RegisterComponent} />
        <Route
          path={`${this.props.match.path}/forgot-password`}
          exact
          component={ForgotPasswordComponent}
        />
      </Switch>
    );
  }
}
