const dev = {
  production: false,
  // WebApiBaseUrl: "/api/v1",
  WebApiBaseUrl: "https://7y4jn76bh7.execute-api.ap-south-1.amazonaws.com/Prod/api/v1"
}

const prod = {
  production: true,
  WebApiBaseUrl: ""
}

export const environment  = process.env.NODE_ENV === 'development' ? dev : prod;