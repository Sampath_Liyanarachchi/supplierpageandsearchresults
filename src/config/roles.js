import React from 'react';
import {
	UploadOutlined,
	MailOutlined,
  } from "@ant-design/icons";

const components = {
	customers: {
		component: 'Customers',
		url: '/customers',
		title: 'Customers',
		icon: <UploadOutlined />,
		visible: true,
		isGroup: false,
	},
	billing: {
		component: 'Billing',
		url: '/billing',
		title: 'Billing',
		icon: <MailOutlined />,
		visible: true,
		isGroup: false,
	},
	profile: {
		component: 'ProfileSettings',
		url: '/profile',
		title: 'Profile Settings',
		icon: <MailOutlined />,
		visible: false,
		isGroup: false,
	},
	customer: {
		component: 'Customer',
		url: '/customers/:id',
		title: 'Customer',
		icon: <UploadOutlined />,
		visible: false,
		isGroup: false,
	},
};

const rolesConfig = {
	FabLanka: {
		routes: [...Object.values(components)]
	},
	common: {
		routes: [
            {
                component: 'Dashboard',
                url: '/dashboard',
                title: 'Dashboard',
                icon: <UploadOutlined />,
				module: 1,
				visible: true
			},
		]
	}
};

export { rolesConfig };