import axios from "axios";
import { environment } from "../environments/environment";

const baseurl = environment.WebApiBaseUrl;


export default axios.create({
  baseURL: baseurl,
  responseType: "json"
});